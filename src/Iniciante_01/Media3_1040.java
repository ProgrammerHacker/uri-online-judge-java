package Iniciante_01;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class Media3_1040 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        DecimalFormat decimalFormat = new DecimalFormat("0.0");

        double nota1, nota2, nota3, nota4, media, notaFinal, mediaFinal;
        String valores[], valor;

        valor = sc.nextLine();

        valores = valor.split(" ");

        nota1 = Double.parseDouble(valores[0]);
        nota2 = Double.parseDouble(valores[1]);
        nota3 = Double.parseDouble(valores[2]);
        nota4 = Double.parseDouble(valores[3]);

        media = ((nota1 * 2) + (nota2 * 3) + (nota3 * 4) + (nota4 * 1)) / 10.0;
        System.out.println("Media: " + decimalFormat.format(media));

        if (media >= 7) {
            System.out.println("Aluno aprovado.");
        } else if (media < 5) {
            System.out.println("Aluno reprovado.");
        } else if (media >= 5 && media < 7) {
            System.out.println("Aluno em exame.");

            notaFinal = sc.nextDouble();
            System.out.println("Nota do exame: " + decimalFormat.format(notaFinal));
            mediaFinal = (media + notaFinal) / 2;

            if (mediaFinal >= 5) {
                System.out.println("Aluno aprovado.");
            } else {
                System.out.println("Aluno reprovado");
            }
            System.out.println("Media final: " + decimalFormat.format(mediaFinal));
        }
    }
}
