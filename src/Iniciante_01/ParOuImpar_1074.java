package Iniciante_01;

import java.io.IOException;
import java.util.Scanner;

public class ParOuImpar_1074 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        sc = new Scanner(System.in);

        int n, valor;

        n = sc.nextInt();

        for (int i = 0; i < n; i++) {
            valor = sc.nextInt();
            if (valor == 0) {
                System.out.println("NULL");
            } else if (valor % 2 == 0) {
                if (valor > 0) {
                    System.out.println("EVEN POSITIVE");
                } else {
                    System.out.println("EVEN NEGATIVE");
                }
            } else {
                if (valor > 0) {
                    System.out.println("ODD POSITIVE");
                } else {
                    System.out.println("ODD NEGATIVE");
                }
            }
        }
    }
}
