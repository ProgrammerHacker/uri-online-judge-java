package Iniciante_01;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class Multiplos_1044 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        int valor1, valor2;
        String valores[], valor;

        valor = sc.nextLine();

        valores = valor.split(" ");

        valor1 = Integer.parseInt(valores[0]);
        valor2 = Integer.parseInt(valores[1]);

        if (valor2 % valor1 == 0 || valor1 % valor2 == 0) {
            System.out.println("Sao Multiplos");
        } else {
            System.out.println("Nao sao Multiplos");
        }
    }
}
