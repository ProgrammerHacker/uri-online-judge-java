package Iniciante_01;

import java.io.IOException;
import java.util.Scanner;

public class ParesEntreCincoNumeros_1065 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        sc = new Scanner(System.in);

        int cont = 0, n;

        for(int i = 0; i<5; i++){
            n = sc.nextInt();
            if (n % 2 == 0){
                cont++;
            }
        }
        System.out.println(cont + " valores pares");
    }
}
