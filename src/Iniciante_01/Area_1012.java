package Iniciante_01;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class Area_1012 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);

        sc = new Scanner(System.in);

        DecimalFormat decimalFormat = new DecimalFormat("0.000");

        String valor, valores[];
        double areaTriangulo, areaCirculo, areaTrapezio, areaQuadrado, areaRetangulo, a, b, c, pi = 3.14159;

        valor = sc.nextLine();

        valores = valor.split(" ");

        a = Double.parseDouble(valores[0]);
        b = Double.parseDouble(valores[1]);
        c = Double.parseDouble(valores[2]);

        areaTriangulo = (a * c) / 2;
        areaCirculo = pi * Math.pow(c, 2);
        areaTrapezio = (c * (a + b)) / 2;
        areaQuadrado = Math.pow(b, 2);
        areaRetangulo = a * b;

        System.out.println("TRIANGULO: " + decimalFormat.format(areaTriangulo));
        System.out.println("CIRCULO: " + decimalFormat.format(areaCirculo));
        System.out.println("TRAPEZIO: " + decimalFormat.format(areaTrapezio));
        System.out.println("QUADRADO: " + decimalFormat.format(areaQuadrado));
        System.out.println("RETANGULO: " + decimalFormat.format(areaRetangulo));

    }
}