package Iniciante_01;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class CoordenadasDeUmPonto_1041 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        double x, y;
        String valores[], valor;

        valor = sc.nextLine();

        valores = valor.split(" ");

        x = Double.parseDouble(valores[0]);
        y = Double.parseDouble(valores[1]);

        if (x == 0 && y == 0) {
            System.out.println("Origem");
        } else if (x != 0 && y == 0) {
            System.out.println("Eixo X");
        } else if (y != 0 && x == 0) {
            System.out.println("Eixo Y");
        } else if (x > 0 && y > 0) {
            System.out.println("Q1");
        } else if (x < 0 && y > 0) {
            System.out.println("Q2");
        } else if (x < 0 && y < 0) {
            System.out.println("Q3");
        } else if (x > 0 && y < 0) {
            System.out.println("Q4");
        }
    }
}
