package Iniciante_01;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class Lanche_1038 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        DecimalFormat decimalFormat = new DecimalFormat("0.00");

        int codigo, quant;
        double total;
        String valores[], valor;

        valor = sc.nextLine();

        valores = valor.split(" ");

        codigo = Integer.parseInt(valores[0]);
        quant = Integer.parseInt(valores[1]);

        if (codigo == 1) {
            total = 4.0 * quant;
            System.out.println("Total: R$ " + decimalFormat.format(total));
        }
        if (codigo == 2) {
            total = 4.5 * quant;
            System.out.println("Total: R$ " + decimalFormat.format(total));
        }
        if (codigo == 3) {
            total = 5.0 * quant;
            System.out.println("Total: R$ " + decimalFormat.format(total));
        }
        if (codigo == 4) {
            total = 2.0 * quant;
            System.out.println("Total: R$ " + decimalFormat.format(total));
        }
        if (codigo == 5) {
            total = 1.5 * quant;
            System.out.println("Total: R$ " + decimalFormat.format(total));
        }
    }
}
