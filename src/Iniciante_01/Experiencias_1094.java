package Iniciante_01;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class Experiencias_1094 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        DecimalFormat decimalFormat = new DecimalFormat("0.00");

        int n, quant, total = 0, coelho = 0, sapo = 0, rato = 0;
        String tipo;
        double totalCoelho, totalSapo, totalRato;

        n = sc.nextInt();

        while (n > 0) {
            quant = sc.nextInt();
            tipo = sc.next();

            if (tipo.equals("C")) {
                coelho = coelho + quant;
            } else if (tipo.equals("S")) {
                sapo = sapo + quant;
            } else if (tipo.equals("R")) {
                rato = rato + quant;
            }

            total = total + quant;
            n--;
        }

        System.out.println("Total: " + total + " cobaias");
        System.out.println("Total de coelhos: " + coelho);
        System.out.println("Total de ratos: " + rato);
        System.out.println("Total de sapos: " + sapo);

        totalCoelho = (Double.parseDouble(decimalFormat.format(coelho)) / total) * 100.0;
        totalSapo = (Double.parseDouble(decimalFormat.format(sapo)) / total) * 100.0;
        totalRato = (Double.parseDouble(decimalFormat.format(rato)) / total) * 100.0;

        System.out.println("Percentual de coelhos: " + decimalFormat.format(totalCoelho) + " %");
        System.out.println("Percentual de ratos: " + decimalFormat.format(totalRato) + " %");
        System.out.println("Percentual de sapos: " + decimalFormat.format(totalSapo) + " %");
    }
}
