package Iniciante_01;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class PositivosEMedia_1064 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        double n, soma = 0.0, media;
        int cont = 0;

        for (int i = 0; i < 6; i++) {
            n = sc.nextDouble();
            if (n > 0) {
                cont++;
                soma = soma + n;
            }
        }

        media = soma / cont;

        System.out.println(cont + " valores positivos");
        System.out.println(media);
    }
}
