package Iniciante_01;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class AumentoDeSalario_1048 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        DecimalFormat decimalFormat = new DecimalFormat("0.00");

        double salario, salarioFinal;

        salario = sc.nextDouble();

        if (salario >= 0 && salario <= 400) {
            salarioFinal = salario + (salario * (15.0 / 100));
            System.out.println("Novo salario: " + decimalFormat.format(salarioFinal));
            System.out.println("Reajuste ganho: " + decimalFormat.format((salario * (15.0 / 100))));
            System.out.println("Em percentual: 15 %");
        } else if (salario > 400 && salario <= 800) {
            salarioFinal = salario + (salario * (12.0 / 100));
            System.out.println("Novo salario: " + decimalFormat.format(salarioFinal));
            System.out.println("Reajuste ganho: " + decimalFormat.format((salario * (12.0 / 100))));
            System.out.println("Em percentual: 12 %");
        } else if (salario > 800 && salario <= 1200) {
            salarioFinal = salario + (salario * (10.0 / 100));
            System.out.println("Novo salario: " + decimalFormat.format(salarioFinal));
            System.out.println("Reajuste ganho: " + decimalFormat.format((salario * (10.0 / 100))));
            System.out.println("Em percentual: 10 %");
        } else if (salario > 1200 && salario <= 2000) {
            salarioFinal = salario + (salario * (7.0 / 100));
            System.out.println("Novo salario: " + decimalFormat.format(salarioFinal));
            System.out.println("Reajuste ganho: " + decimalFormat.format((salario * (7.0 / 100))));
            System.out.println("Em percentual: 7 %");
        } else if (salario > 2000) {
            salarioFinal = salario + (salario * (4.0 / 100));
            System.out.println("Novo salario: " + decimalFormat.format(salarioFinal));
            System.out.println("Reajuste ganho: " + decimalFormat.format((salario * (4.0 / 100))));
            System.out.println("Em percentual: 4 %");
        }
    }
}
