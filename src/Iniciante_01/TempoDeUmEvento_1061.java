package Iniciante_01;

import java.io.IOException;
import java.util.Scanner;

public class TempoDeUmEvento_1061 {
    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        sc = new Scanner(System.in);

        String valorD1[], valorD2[], valorH1[], valorH2[], d1, d2, h1, h2;
        int dia, hora, minuto, segundo;

        d1 = sc.nextLine();
        h1 = sc.nextLine();
        d2 = sc.nextLine();
        h2 = sc.nextLine();

        valorD1 = d1.split(" ");
        valorH1 = h1.split(" : ");
        valorD2 = d2.split(" ");
        valorH2 = h2.split(" : ");

        dia = Integer.parseInt(valorD2[1]) - Integer.parseInt(valorD1[1]);
        hora = Integer.parseInt(valorH2[0]) - Integer.parseInt(valorH1[0]);
        minuto = Integer.parseInt(valorH2[1]) - Integer.parseInt(valorH1[1]);
        segundo = Integer.parseInt(valorH2[2]) - Integer.parseInt(valorH1[2]);

        if (segundo < 0) {
            segundo += 60;
            minuto--;
        }
        if (minuto < 0) {
            minuto += 60;
            hora--;
        }
        if (hora < 0) {
            hora += 24;
            dia--;
        }

        System.out.println(dia + " dia(s)");
        System.out.println(hora + " hora(s)");
        System.out.println(minuto + " minuto(s)");
        System.out.println(segundo + " segundo(s)");
    }
}
