package Iniciante_01;

import java.io.IOException;

public class SequenciaIJ1_1095 {

    public static void main(String[] args) throws IOException {
        int n1 = 1, n2 = 60;
        for (int i = 0; i <= 12; i++) {
            System.out.println("I=" + n1 + " J=" + n2);
            n1 = n1 + 3;
            n2 = n2 - 5;
        }
    }
}
