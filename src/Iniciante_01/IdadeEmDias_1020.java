package Iniciante_01;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class IdadeEmDias_1020 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        int valor, ano, mes, dias;

        valor = sc.nextInt();

        ano = valor / 365;
        mes = (valor % 365) / 30;
        dias = (valor - (ano * 365)) - (mes * 30);

        System.out.println(ano + " ano(s)");
        System.out.println(mes + " mes(es)");
        System.out.println(dias + " dia(s)");

    }
}
