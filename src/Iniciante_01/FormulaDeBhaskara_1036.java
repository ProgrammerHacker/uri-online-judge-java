package Iniciante_01;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class FormulaDeBhaskara_1036 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        DecimalFormat decimalFormat = new DecimalFormat("0.00000");

        double a, b, c, delta, x1, x2;
        String valores[], valor;

        valor = sc.nextLine();

        valores = valor.split(" ");

        a = Double.parseDouble(valores[0]);
        b = Double.parseDouble(valores[1]);
        c = Double.parseDouble(valores[2]);

        delta = Math.pow(b, 2) - (4 * a * c);

        if (delta <= 0 || a == 0) {
            System.out.println("Impossivel calcular");
        } else {
            x1 = ((-1 * b) + Math.sqrt(delta)) / (2 * a);
            x2 = ((-1 * b) - Math.sqrt(delta)) / (2 * a);

            System.out.println("R1 = " + decimalFormat.format(x1));
            System.out.println("R2 = " + decimalFormat.format(x2));
        }
    }
}
