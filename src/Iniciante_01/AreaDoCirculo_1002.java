package Iniciante_01;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class AreaDoCirculo_1002 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);

        sc = new Scanner(System.in);

        DecimalFormat decimalFormat = new DecimalFormat("0.0000");

        double pi = 3.14159;
        double raio, area;

        raio = sc.nextDouble();

        area = pi * Math.pow(raio, 2);

        System.out.println("A="+decimalFormat.format(area));

    }
}
