package Iniciante_01;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class Consumo_1014 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        DecimalFormat decimalFormat = new DecimalFormat("0.000");

        int distancia;
        double conbustivel, kml;

        distancia = sc.nextInt();
        conbustivel = sc.nextDouble();

        kml = distancia / conbustivel;

        System.out.println(decimalFormat.format(kml) + " km/l");
    }
}
