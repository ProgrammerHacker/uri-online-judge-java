package Iniciante_01;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class ImpostoDeRenda_1051 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        DecimalFormat decimalFormat = new DecimalFormat("0.00");

        double salario;

        salario = sc.nextDouble();

        if (salario <= 2000.0) {
            System.out.println("Isento");
        } else if (salario < 3000.0) {
            salario = (salario - 2000.0) * (8.0 / 100);
            System.out.println("R$ " + decimalFormat.format(salario));
        } else if (salario < 4500.0) {
            salario = (1000.0 * (8.0 / 100)) + (salario - 3000) * (18.0 / 100);
            System.out.println("R$ " + decimalFormat.format(salario));
        } else {
            salario = (1000.0 * (8.0 / 100)) + (1500.0 * (18.0 / 100)) + (salario - 4500) * (28.0 / 100);
            System.out.println("R$ " + decimalFormat.format(salario));
        }
    }
}
