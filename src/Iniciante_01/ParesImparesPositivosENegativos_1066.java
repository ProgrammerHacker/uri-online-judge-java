package Iniciante_01;

import java.io.IOException;
import java.util.Scanner;

public class ParesImparesPositivosENegativos_1066 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        sc = new Scanner(System.in);

        int par = 0, impar = 0, positivo = 0, negativo = 0, n;

        for (int i = 0; i < 5; i++) {
            n = sc.nextInt();

            if (n % 2 == 0) {
                par++;
            } else {
                impar++;
            }

            if (n > 0) {
                positivo++;
            }
            if (n < 0) {
                negativo++;
            }
        }
        System.out.println(par + " valor(es) par(es)");
        System.out.println(impar + " valor(es) impar(es)");
        System.out.println(positivo + " valor(es) positivo(s)");
        System.out.println(negativo + " valor(es) negativo(s)");
    }
}
