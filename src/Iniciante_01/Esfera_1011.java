package Iniciante_01;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class Esfera_1011 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);

        sc = new Scanner(System.in);

        DecimalFormat decimalFormat = new DecimalFormat("0.000");

        double pi = 3.14159, esfera;

        int raio = sc.nextInt();

        esfera = (4.0 / 3.0) * pi * Math.pow(raio, 3);

        System.out.println("VOLUME = " + decimalFormat.format(esfera));

    }
}