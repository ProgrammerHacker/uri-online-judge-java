package Iniciante_01;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class CalculoSimples_1010 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);

        sc = new Scanner(System.in);

        DecimalFormat decimalFormat = new DecimalFormat("0.00");

        String p1, p2, produto1[], produto2[];

        int codigo1, numeroPeca1, codigo2, numeroPeca2;
        double valor1, valor2, pagar;

        p1 = sc.nextLine();
        p2 = sc.nextLine();

        produto1 = p1.split(" ");
        produto2 = p2.split(" ");

        codigo1 = Integer.parseInt(produto1[0]);
        numeroPeca1 = Integer.parseInt(produto1[1]);
        valor1 = Double.parseDouble(produto1[2]);

        codigo2 = Integer.parseInt(produto2[0]);
        numeroPeca2 = Integer.parseInt(produto2[1]);
        valor2 = Double.parseDouble(produto2[2]);

        pagar = (numeroPeca1 * valor1) + (numeroPeca2 * valor2);

        System.out.println("VALOR A PAGAR: R$ " + decimalFormat.format(pagar));

    }
}