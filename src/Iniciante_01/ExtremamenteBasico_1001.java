package Iniciante_01;

import java.io.IOException;
import java.util.Scanner;

public class ExtremamenteBasico_1001 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        sc = new Scanner(System.in);

        int n1, n2, soma;

        n1 = sc.nextInt();
        n2 = sc.nextInt();

        soma = n1+n2;

        System.out.println("X = " + soma);

    }
}