package Iniciante_01;

import java.io.IOException;
import java.util.Scanner;

public class SeisNumerosImpares_1070 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        sc = new Scanner(System.in);

        int n, cont = 0;
        boolean impar = true;

        n = sc.nextInt();

        while (impar) {
            if (n % 2 != 0) {
                System.out.println(n);
                cont = cont + 1;
            }
            if (cont == 6) {
                impar = false;
            }
            n = n + 1;
        }
    }
}
