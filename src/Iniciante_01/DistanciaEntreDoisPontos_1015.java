package Iniciante_01;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class DistanciaEntreDoisPontos_1015 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        DecimalFormat decimalFormat = new DecimalFormat("0.0000");

        String valores1[], valores2[], valor1, valor2;
        double y1, y2, x1, x2, distancia;

        valor1 = sc.nextLine();
        valor2 = sc.nextLine();

        valores1 = valor1.split(" ");
        valores2 = valor2.split(" ");

        x1 = Double.parseDouble(valores1[0]);
        y1 = Double.parseDouble(valores1[1]);
        x2 = Double.parseDouble(valores2[0]);
        y2 = Double.parseDouble(valores2[1]);

        distancia = Math.sqrt(Math.pow((x2-x1), 2) + Math.pow((y2-y1), 2));

        System.out.println(decimalFormat.format(distancia));
    }
}
