package Iniciante_01;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class SalarioComBonus_1009 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);

        sc = new Scanner(System.in);

        DecimalFormat decimalFormat = new DecimalFormat("0.00");

        String nome;

        double salarioFinal, salarioFixo, vendasEfetuadas;

        nome = sc.nextLine();
        salarioFixo = sc.nextDouble();
        vendasEfetuadas = sc.nextDouble();

        salarioFinal = salarioFixo + (vendasEfetuadas * 0.15);

        System.out.println("TOTAL = R$ " + decimalFormat.format(salarioFinal));

    }
}