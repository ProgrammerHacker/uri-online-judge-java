package Iniciante_01;

import java.io.IOException;
import java.util.Scanner;

public class Intervalo2_1072 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        sc = new Scanner(System.in);

        int n = 0, dentro = 0, fora = 0, entrada, valor;

        valor = sc.nextInt();
        ;

        while (n < valor) {
            entrada = sc.nextInt();
            if (entrada >= 10 && entrada <= 20) {
                dentro += 1;
            } else {
                fora += 1;
            }
            n += 1;
        }

        System.out.println(dentro + " in");
        System.out.println(fora + " out");
    }
}
