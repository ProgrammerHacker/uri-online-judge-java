package Iniciante_01;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class TiposDeTriangulos_1045 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        double valor1, valor2, valor3, aux;
        String valores[], valor;

        valor = sc.nextLine();

        valores = valor.split(" ");

        valor1 = Double.parseDouble(valores[0]);
        valor2 = Double.parseDouble(valores[1]);
        valor3 = Double.parseDouble(valores[2]);

        if (valor1 < valor2) {
            aux = valor1;
            valor1 = valor2;
            valor2 = aux;
        }
        if (valor2 < valor3) {
            aux = valor2;
            valor2 = valor3;
            valor3 = aux;
        }
        if (valor1 < valor2) {
            aux = valor1;
            valor1 = valor2;
            valor2 = aux;
        }

        if (valor1 >= (valor2 + valor3)) {
            System.out.println("NAO FORMA TRIANGULO");
        } else {
            if (Math.pow(valor1, 2) == Math.pow(valor2, 2) + Math.pow(valor3, 2)) {
                System.out.println("TRIANGULO RETANGULO");
            } else if (Math.pow(valor1, 2) > Math.pow(valor2, 2) + Math.pow(valor3, 2)) {
                System.out.println("TRIANGULO OBTUSANGULO");
            } else if (Math.pow(valor1, 2) < Math.pow(valor2, 2) + Math.pow(valor3, 2)) {
                System.out.println("TRIANGULO ACUTANGULO");
            }
            if (valor1 == valor2 && valor1 == valor3) {
                System.out.println("TRIANGULO EQUILATERO");
            } else if (valor1 == valor2 || valor2 == valor3 || valor3 == valor1) {
                System.out.println("TRIANGULO ISOSCELES");
            }
        }
    }
}
