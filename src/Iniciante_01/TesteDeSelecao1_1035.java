package Iniciante_01;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class TesteDeSelecao1_1035 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        int valor1, valor2, valor3, valor4;
        String valores[], valor;

        valor = sc.nextLine();

        valores = valor.split(" ");

        valor1 = Integer.parseInt(valores[0]);
        valor2 = Integer.parseInt(valores[1]);
        valor3 = Integer.parseInt(valores[2]);
        valor4 = Integer.parseInt(valores[3]);

        if (valor2 > valor3 && valor4 > valor1 && (valor3 + valor4) > (valor1 + valor2) && valor3 > 0 && valor4 > 0 && valor1 % 2 == 0) {
            System.out.println("Valores aceitos");
        } else {
            System.out.println("Valores nao aceitos");
        }
    }
}
