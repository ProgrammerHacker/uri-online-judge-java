package Iniciante_01;

import java.io.IOException;
import java.util.Scanner;

public class SomaDeImparesConsecutivosII_1099 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        sc = new Scanner(System.in);

        int n, n1, n2, aux, soma;

        n = sc.nextInt();

        for (int i = 1; i <= n; i++) {
            n1 = sc.nextInt();
            n2 = sc.nextInt();

            if (n1 > n2) {
                aux = n1;
                n1 = n2;
                n2 = aux;
            }

            soma = 0;

            while (n1 < n2) {
                n1 += 1;
                if (n1 == n2) {
                    break;
                } else if (n1 % 2 == 1) {
                    soma = soma + n1;
                }
            }
            System.out.println(soma);
        }
    }
}
