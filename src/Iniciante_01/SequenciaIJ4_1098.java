package Iniciante_01;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;

public class SequenciaIJ4_1098 {

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        DecimalFormat decimalFormat = new DecimalFormat("0.0");

        float n = 0, m = 1;

        while (n < 2.2) {

            if (n == 1.0 || Float.parseFloat(decimalFormat.format(n)) == 2.0 || n == 0.0) {
                System.out.println("I=" + (int) n + " J=" + (int) m);
                System.out.println("I=" + (int) n + " J=" + (int) (m + 1));
                System.out.println("I=" + (int) n + " J=" + (int) (m + 2));
            } else {
                System.out.println("I=" + decimalFormat.format(n) + " J=" + decimalFormat.format(m));
                System.out.println("I=" + decimalFormat.format(n) + " J=" + decimalFormat.format((m + 1)));
                System.out.println("I=" + decimalFormat.format(n) + " J=" + decimalFormat.format((m + 2)));
            }

            n += 0.2;
            m += 0.2;
        }
    }
}
