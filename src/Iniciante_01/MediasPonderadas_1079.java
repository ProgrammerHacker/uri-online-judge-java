package Iniciante_01;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class MediasPonderadas_1079 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        DecimalFormat decimalFormat = new DecimalFormat("0.0");

        int n;
        double n1, n2, n3, ponderada;

        n = sc.nextInt();

        while (n > 0) {
            n1 = sc.nextDouble();
            n2 = sc.nextDouble();
            n3 = sc.nextDouble();

            ponderada = ((n1 * 2) + (n2 * 3) + (n3 * 5)) / 10;

            System.out.println(decimalFormat.format(ponderada));
            n--;

        }
    }
}
