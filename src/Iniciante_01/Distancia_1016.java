package Iniciante_01;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class Distancia_1016 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        int minutos, kmh;

        kmh = sc.nextInt();

        minutos = (kmh*60)/30;

        System.out.println(minutos + " minutos");
    }
}
