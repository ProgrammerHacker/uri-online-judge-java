package Iniciante_01;

import java.io.IOException;
import java.util.Scanner;

public class QuadradoDePares_1073 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        sc = new Scanner(System.in);

        int cont = 1, valor;
        double res;

        valor = sc.nextInt();

        while (cont <= valor) {
            if (cont % 2 == 0) {
                res = Math.pow(cont, 2);
                System.out.println(cont + "^2 = " + (int) res);
            }
            cont++;
        }
    }
}
