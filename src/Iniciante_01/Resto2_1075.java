package Iniciante_01;

import java.io.IOException;
import java.util.Scanner;

public class Resto2_1075 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        sc = new Scanner(System.in);

        int n, cont = 1;

        n = sc.nextInt();

        while (cont <= 10000) {
            if ((cont % n) == 2) {
                System.out.println(cont);
            }
            cont++;
        }
    }
}
