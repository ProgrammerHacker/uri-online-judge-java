package Iniciante_01;

import java.io.IOException;

public class SequenciaIJ2_1096 {

    public static void main(String[] args) throws IOException {

        for (int i = 1; i <= 9; i += 2) {
            System.out.println("I=" + i + " J=7");
            System.out.println("I=" + i + " J=6");
            System.out.println("I=" + i + " J=5");
        }
    }
}
