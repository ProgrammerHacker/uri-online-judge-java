package Iniciante_01;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class Triangulo_1043 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        DecimalFormat decimalFormat = new DecimalFormat("0.0");

        double valor1, valor2, valor3, area, perimetro;
        String valores[], valor;

        valor = sc.nextLine();

        valores = valor.split(" ");

        valor1 = Double.parseDouble(valores[0]);
        valor2 = Double.parseDouble(valores[1]);
        valor3 = Double.parseDouble(valores[2]);

        if (valor1 + valor2 > valor3 && valor2 + valor3 > valor1 && valor1 + valor3 > valor2) {
            perimetro = valor1 + valor2 + valor3;
            System.out.println("Perimetro = " + decimalFormat.format(perimetro));
        } else {
            area = (valor3 * (valor1 + valor2)) / 2;
            System.out.println("Area = " + decimalFormat.format(area));
        }
    }
}
