package Iniciante_01;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class Cedulas_1018 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        int valor, cem, cinquenta, vinte, dez, cinco, dois, um;

        valor = sc.nextInt();

        cem = valor / 100;
        cinquenta = (valor % 100) / 50;
        vinte = ((valor % 100) % 50) / 20;
        dez = (((valor % 100) % 50) % 20) / 10;
        cinco = ((((valor % 100) % 50) % 20) % 10) / 5;
        dois = (((((valor % 100) % 50) % 20) % 10) % 5) / 2;
        um = ((((((valor % 100) % 50) % 20) % 10) % 5) % 2) / 1;

        System.out.println(valor);
        System.out.println(cem + " nota(s) de R$ 100,00");
        System.out.println(cinquenta + " nota(s) de R$ 50,00");
        System.out.println(vinte + " nota(s) de R$ 20,00");
        System.out.println(dez + " nota(s) de R$ 10,00");
        System.out.println(cinco + " nota(s) de R$ 5,00");
        System.out.println(dois + " nota(s) de R$ 2,00");
        System.out.println(um + " nota(s) de R$ 1,00");
    }
}
