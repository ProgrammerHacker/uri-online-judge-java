package Iniciante_01;

import java.io.IOException;

public class SequenciaIJ3_1097 {

    public static void main(String[] args) throws IOException {
        int n1 = 7;

        for (int i=1; i<=9; i+=2){
            System.out.println("I=" + i + " J=" + n1);
            System.out.println("I=" + i + " J=" + (n1-1));
            System.out.println("I=" + i + " J=" + (n1-2));
            n1 +=2;
        }
    }
}
