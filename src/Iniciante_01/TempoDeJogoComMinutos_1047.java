package Iniciante_01;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class TempoDeJogoComMinutos_1047 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        int valor1, valor2, valor3, valor4, hora, minuto;
        String valores[], valor;

        valor = sc.nextLine();

        valores = valor.split(" ");

        valor1 = Integer.parseInt(valores[0]);
        valor2 = Integer.parseInt(valores[1]);
        valor3 = Integer.parseInt(valores[2]);
        valor4 = Integer.parseInt(valores[3]);

        hora = (valor3 - valor1);
        if ((valor3 - valor1) < 0) {
            hora = 24 + (valor3 - valor1);
        }

        minuto = (valor4 - valor2);
        if ((valor4 - valor2) < 0) {
            minuto = 60 + (valor4 - valor2);
            hora--;
        }

        if (valor3 == valor1 && valor4 == valor2) {
            System.out.println("O JOGO DUROU 24 HORA(S) E 0 MINUTO(S)");
        } else {
            System.out.println("O JOGO DUROU " + hora + " HORA(S) E " + minuto + " MINUTO(S)");
        }
    }
}
