package Iniciante_01;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class TempoDeJogo_1046 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        int valor1, valor2, hora;
        String valores[], valor;

        valor = sc.nextLine();

        valores = valor.split(" ");

        valor1 = Integer.parseInt(valores[0]);
        valor2 = Integer.parseInt(valores[1]);

        hora = valor2 - valor1;

        if ((valor2 - valor1) < 0) {
            hora = 24 + (valor2 - valor1);
            System.out.println("O JOGO DUROU " + hora + " HORA(S)");
        } else if (valor2 == valor1) {
            System.out.println("O JOGO DUROU 24 HORA(S)");
        } else {
            System.out.println("O JOGO DUROU " + hora + " HORA(S)");
        }
    }
}
