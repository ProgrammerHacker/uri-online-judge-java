package Iniciante_01;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class Media2_1006 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);

        sc = new Scanner(System.in);

        DecimalFormat decimalFormat = new DecimalFormat("0.0");

        double media, nota1, nota2, nota3;

        nota1 = sc.nextDouble();
        nota2 = sc.nextDouble();
        nota3 = sc.nextDouble();

        media = ((nota1 * 2) + (nota2 * 3) + (nota3 * 5)) / 10;

        System.out.println("MEDIA = " + decimalFormat.format(media));

    }
}