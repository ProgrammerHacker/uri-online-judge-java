package Iniciante_01;

import java.io.IOException;
import java.util.Scanner;

public class SequenciaDeNumerosESoma_1101 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        sc = new Scanner(System.in);

        int n1, n2, soma, aux;
        String sequencia;

        n1 = sc.nextInt();
        n2 = sc.nextInt();

        while (n1>0 && n2>0){
            sequencia = "";
            soma = 0;
            if(n1>n2){
                aux = n1;
                n1 = n2;
                n2 = aux;
            }

            for (int i = n1; i<=n2; i++){
                sequencia += String.valueOf(i)+" ";
                soma = soma+i;
            }

            System.out.println(sequencia+"Sum="+soma);
            n1 = sc.nextInt();
            n2 = sc.nextInt();
        }
    }
}
