package Iniciante_01;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class ConversaoDeTempo_1019 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        int valor, hora, minuto, segundo;

        valor = sc.nextInt();

        hora = (valor / 60) / 60;
        minuto = (valor / 60) % 60;
        segundo = valor % 60;

        System.out.println(hora + ":" + minuto + ":" + segundo);
    }
}
