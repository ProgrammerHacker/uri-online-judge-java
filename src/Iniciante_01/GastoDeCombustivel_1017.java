package Iniciante_01;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class GastoDeCombustivel_1017 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        DecimalFormat decimalFormat = new DecimalFormat("0.000");

        int valor1, valor2;
        double litros;

        valor1 = sc.nextInt();
        valor2 = sc.nextInt();

        litros = (valor1 * valor2)/12.0;

        System.out.println(decimalFormat.format(litros));
    }
}
