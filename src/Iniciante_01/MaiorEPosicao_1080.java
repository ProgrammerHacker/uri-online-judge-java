package Iniciante_01;

import java.io.IOException;
import java.util.Scanner;

public class MaiorEPosicao_1080 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        sc = new Scanner(System.in);

        int maior = 0, posicao = 0, valor;

        for (int i = 1; i<=100; i++){
            valor = sc.nextInt();
            if (valor > maior){
                maior = valor;
                posicao = i;
            }
        }

        System.out.println(maior + "\n" + posicao);
    }
}
