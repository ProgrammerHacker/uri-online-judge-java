package Iniciante_01;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class Salario_1008 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);

        sc = new Scanner(System.in);

        DecimalFormat decimalFormat = new DecimalFormat("0.00");

        double salario, valorHora;
        int numFuncionario, horasTrab;

        numFuncionario = sc.nextInt();
        horasTrab = sc.nextInt();
        valorHora = sc.nextDouble();

        salario = valorHora * horasTrab;

        System.out.println("NUMBER = " + numFuncionario);
        System.out.println("SALARY = U$ " + decimalFormat.format(salario));

    }
}