package Iniciante_01;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class SortSimples_1042 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        int n1, n2, n3, valor1, valor2, valor3, aux;
        String valores[], valor;

        valor = sc.nextLine();

        valores = valor.split(" ");

        n1 = Integer.parseInt(valores[0]);
        n2 = Integer.parseInt(valores[1]);
        n3 = Integer.parseInt(valores[2]);

        valor1 = n1;
        valor2 = n2;
        valor3 = n3;

        if (valor1 < valor2) {
            aux = valor1;
            valor1 = valor2;
            valor2 = aux;
        }
        if (valor2 < valor3) {
            aux = valor2;
            valor2 = valor3;
            valor3 = aux;
        }
        if (valor1 < valor2) {
            aux = valor1;
            valor1 = valor2;
            valor2 = aux;
        }

        System.out.println(valor3);
        System.out.println(valor2);
        System.out.println(valor1 + "\n");
        System.out.println(n1);
        System.out.println(n2);
        System.out.println(n3);
    }
}
