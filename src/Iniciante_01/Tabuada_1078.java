package Iniciante_01;

import java.io.IOException;
import java.util.Scanner;

public class Tabuada_1078 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        sc = new Scanner(System.in);

        int n, fixo;
        n = sc.nextInt();

        fixo = n;

        for (int i = 1; i <= 10; i++) {
            System.out.println(i + " x " + fixo + " = " + (i * n));
        }
    }
}
