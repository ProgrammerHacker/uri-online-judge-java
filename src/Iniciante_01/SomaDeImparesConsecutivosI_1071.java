package Iniciante_01;

import java.io.IOException;
import java.util.Scanner;

public class SomaDeImparesConsecutivosI_1071 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        sc = new Scanner(System.in);

        int n1, n2, impar = 0, aux;
        boolean verdade = true;

        n1 = sc.nextInt();
        n2 = sc.nextInt();

        if (n1 > n2) {
            aux = n1;
            n1 = n2;
            n2 = aux;
        }

        while (verdade) {
            n1++;
            if (n1 >= n2) {
                verdade = false;
            } else if (n1 % 2 != 0) {
                impar = impar + n1;
            }
        }
        System.out.println(impar);
    }
}
