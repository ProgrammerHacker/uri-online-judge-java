package Iniciante_01;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class NotasEMoedas_1021 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        sc = new Scanner(System.in);

        int aux, aux1, cem, cinquenta, vinte, dez, cinco, dois, um, cinquentaC, vinteCincoC,
                dezC, cincoC, umC;
        double dinheiro;

        dinheiro = sc.nextDouble();

        cem = (int) (dinheiro / 100);
        aux = cem;
        aux = (int) (dinheiro % 100);
        cinquenta = aux / 50;
        aux = (aux % 50);
        vinte = aux / 20;
        aux = (aux % 20);
        dez = aux / 10;
        aux = (aux % 10);
        cinco = aux / 5;
        aux = (aux % 5);
        dois = aux / 2;
        aux = (aux % 2);

        um = aux / 1;
        aux1 = (int) (dinheiro * 100);
        aux1 = aux1 % 100;
        cinquentaC = aux1 / 50;
        aux1 = aux1 % 50;
        vinteCincoC = aux1 / 25;
        aux1 = aux1 % 25;
        dezC = aux1 / 10;
        aux1 = aux1 % 10;
        cincoC = aux1 / 5;
        aux1 = aux1 % 5;
        umC = aux1 / 1;

        System.out.println("NOTAS:");
        System.out.println(cem + " nota(s) de R$ 100.00");
        System.out.println(cinquenta + " nota(s) de R$ 50.00");
        System.out.println(vinte + " nota(s) de R$ 20.00");
        System.out.println(dez + " nota(s) de R$ 10.00");
        System.out.println(cinco + " nota(s) de R$ 5.00");
        System.out.println(dois + " nota(s) de R$ 2.00");
        System.out.println("MOEDAS:");
        System.out.println(um + " moeda(s) de R$ 1.00");
        System.out.println(cinquentaC + " moeda(s) de R$ 0.50");
        System.out.println(vinteCincoC + " moeda(s) de R$ 0.25");
        System.out.println(dezC + " moeda(s) de R$ 0.10");
        System.out.println(cincoC + " moeda(s) de R$ 0.05");
        System.out.println(umC + " moeda(s) de R$ 0.01");

    }
}
