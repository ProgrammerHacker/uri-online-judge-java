package Iniciante_01;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class Media1_1005 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.US);

        sc = new Scanner(System.in);

        DecimalFormat decimalFormat = new DecimalFormat("0.00000");

        double media, nota1, nota2;

        nota1 = sc.nextDouble();
        nota2 = sc.nextDouble();

        media = ((nota1 * 3.5) + (nota2 * 7.5)) / 11;

        System.out.println("MEDIA = " + decimalFormat.format(media));

    }
}