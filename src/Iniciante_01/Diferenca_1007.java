package Iniciante_01;

import java.io.IOException;
import java.util.Scanner;

public class Diferenca_1007 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        sc = new Scanner(System.in);

        int diferenca, n1, n2, n3, n4;

        n1 = sc.nextInt();
        n2 = sc.nextInt();
        n3 = sc.nextInt();
        n4 = sc.nextInt();

        diferenca = (n1 * n2 - n3 * n4);

        System.out.println("DIFERENCA = " + diferenca);

    }
}