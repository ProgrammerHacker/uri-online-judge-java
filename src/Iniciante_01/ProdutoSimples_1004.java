package Iniciante_01;

import java.io.IOException;
import java.util.Scanner;

public class ProdutoSimples_1004 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        sc = new Scanner(System.in);

        int n1, n2, produto;

        n1 = sc.nextInt();
        n2 = sc.nextInt();

        produto = n1*n2;

        System.out.println("PROD = " + produto);

    }
}
