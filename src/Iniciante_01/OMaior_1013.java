package Iniciante_01;

import java.io.IOException;
import java.util.Scanner;

public class OMaior_1013 {

    private static Scanner sc;

    public static void main(String[] args) throws IOException {
        sc = new Scanner(System.in);

        String valores[], valor;
        int valor1, valor2, valor3, maiorAB, maior;

        valor = sc.nextLine();

        valores = valor.split(" ");

        valor1 = Integer.parseInt(valores[0]);
        valor2 = Integer.parseInt(valores[1]);
        valor3 = Integer.parseInt(valores[2]);

        maiorAB = ((valor1 + valor2 + Math.abs(valor1 - valor2))) / 2;
        maior = ((maiorAB + valor3 + Math.abs(maiorAB - valor3))) / 2;

        System.out.println(maior + " eh o maior");
    }
}